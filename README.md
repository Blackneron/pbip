# PBip - README #
---

### Overview ###

The **PBip** tool displays the available IP addresses of a PC. With shortcuts the IP addresses can be copied to the clipboard.

### Screenshot ###

![PBip - Info Window](development/readme/pbip.png "PBip - Info Window")

### Setup ###

* Start the **PBip.exe** executable with a doubleclick.
* **CTRL+1** copies the first IP address to the clipboard.
* **CTRL+2** copies the second IP address to the clipboard.
* **CTRL+3** copies the third IP address to the clipboard.
* **CTRL+4** copies the fourth IP address to the clipboard.
* **CTRL+5** copies all IP addresses to the clipboard.
* **CTRL+X** terminates the application.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBip** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
