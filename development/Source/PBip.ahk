; ======================================================================
;
;                       PBip - Display IP Adresses
;
;                              Version 1.2
;
;               Copyright 2015 / PB-Soft / Patrick Biegel
;
;                            www.pb-soft.com
;
; ======================================================================


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   S E T T I N G S
; ======================================================================

; Enable autotrim.
AutoTrim, On

; Specify that only one instance of this application can run.
#SingleInstance ignore

; Set the script speed.
SetBatchLines, 20ms

; Set the working directory.
SetWorkingDir, %A_ScriptDir%


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   I N F O R M A T I O N
; ======================================================================

; Specify the application name.
ApplName = PBip

; Specify the application version.
ApplVersion = 1.2


; ======================================================================
; D I S P L A Y   I P   A D D R E S S   I N F O R M A T I O N
; ======================================================================

; Initialize the IP address string.
IPstring =

; Check if the first IP address is not empty.
if(A_IPAddress1 <> "0.0.0.0")
{

  ; Add the first IP address to the IP string.
  IPstring = %IPstring%IP-1: %A_IPAddress1%`n
}

; Check if the second IP address is not empty.
if(A_IPAddress2 <> "0.0.0.0") {

  ; Add the second IP address to the IP string.
  IPstring = %IPstring%IP-2: %A_IPAddress2%`n
}

; Check if the third IP address is not empty.
if(A_IPAddress3 <> "0.0.0.0") {

  ; Add the third IP address to the IP string.
  IPstring = %IPstring%IP-3: %A_IPAddress3%`n
}

; Check if the fourth IP address is not empty.
if(A_IPAddress4 <> "0.0.0.0") {

  ; Add the fourth IP address to the IP string.
  IPstring = %IPstring%IP-4: %A_IPAddress4%`n
}

; Check if the IP string is empty.
if(IPstring == "") {

  ; Specify an information message.
  IPstring = ! ! ! ! NO IP ADDRESSES WERE FOUND ! ! ! !
}

; Display an information message with the IP address.
MsgBox, 64, %ApplName% %ApplVersion% - IP Adresses, This computer has the following IP address(es):`n`n%IPstring%

; Copy the first IP address to the clipboard.
clipboard = %A_IPAddress1%


; ======================================================================
; S H O R T C U T S / H O T K E Y S
; ======================================================================

; Keys: CTRL + 1
^1::

  ; Copy the first IP address to the clipboard.
  clipboard = %A_IPAddress1%

  ; Display an information balloon message.
  TrayTip, IP address 1 copied..., %A_IPAddress1%, 10, 1

return

; Keys: CTRL + 2
^2::

  ; Copy the second IP address to the clipboard.
  clipboard = %A_IPAddress2%

  ; Display an information balloon message.
  TrayTip, IP address 2 copied..., %A_IPAddress2%, 10, 1

return

; Keys: CTRL + 3
^3::

  ; Copy the third IP address to the clipboard.
  clipboard = %A_IPAddress3%

  ; Display an information balloon message.
  TrayTip, IP address 3 copied..., %A_IPAddress3%, 10, 1

return

; Keys: CTRL + 4
^4::

  ; Copy the fourth IP address to the clipboard.
  clipboard = %A_IPAddress4%

  ; Display an information balloon message.
  TrayTip, IP address 4 copied..., %A_IPAddress4%, 10, 1

return

; Keys: CTRL + 5
^5::

  ; Copy all the IP addresses to the clipboard.
  clipboard = %A_IPAddress1%`n%A_IPAddress2%`n%A_IPAddress3%`n%A_IPAddress4%

  ; Display an information balloon message.
  TrayTip, All IP addresses copied..., %A_IPAddress1%`n%A_IPAddress2%`n%A_IPAddress3%`n%A_IPAddress4%, 10, 1

return

; Keys: CTRL + x
^x::

  ; Display an information balloon message.
  TrayTip, Exit application..., Please wait..., 10, 1

  ; Sleep some time.
  Sleep, 2000

  ; Exit the application.
  ExitApp

return
